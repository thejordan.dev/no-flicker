package fr.thejordan.noflicker;

import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ChatColorRandomizer {

    public int finished = 1;
    public List<String> usedCombinations = new ArrayList<String>();
    public ArrayList<ChatColor> usedColors = new ArrayList<ChatColor>();
    public Random random = new Random();

    public String generate() {
        StringBuilder builder = new StringBuilder();
        while (builder.toString().equals("") || usedCombinations.contains(builder.toString())) {
            builder = new StringBuilder();
            for (int _c = 0; _c < finished; _c++) {
                int index = random.nextInt(ChatColor.values().length);
                if (!usedColors.isEmpty()) {
                    while (usedColors.get(usedColors.size() - 1).equals(ChatColor.values()[index])) {
                        index = random.nextInt(ChatColor.values().length);
                    }
                }
                builder.append(ChatColor.values()[index]);
                usedColors.add(ChatColor.values()[index]);
            }
        }
        usedCombinations.add(builder.toString());
        if (usedCombinations.size() == ChatColor.values().length*finished) finished++;
        return builder.toString();
    }

}
