# Comment utiliser l'API ?

# I - Initialisation

*Il faut compiler le code avec votre Plugin. Cet API n'est pas un plugin.*

**IMPORTANT**

Pour que le system fonctionne vous devez initialiser le manager pour cela,  
ajoutez juste cette ligne de code dans le onEnable de votre plugin

```Java
public void onEnable() {
    //Votre code...
    new CScoreboardManager(this);
}
```

# II - Création du scoreboard

```Java
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

public class ExempleScoreboard extends CScoreboard {

    // Clé d'identification pour les variables.
    public static String playerName = "player_name";

    public ExempleScoreboard(Player player) {
        // super("ID du scoreboard","Titre à afficher","Joueur lié au scoreboard");
        super("edc_scoreboard", "EDC", player);
    }
    
    public void setup() {
        this.setStatics(">> Nom", 10); // Défini un text immobile (qui ne ce modifiera pas)
        this.setVariable(playerName, " ", 9); // Défini un text variable (qui ce modifie avec une clé)
        this.setEmpty(8); // Créé un espace vide dans le scoreboard
        this.setStatics(">> Joueurs", 7);
        this.setVariable(online_players, " ", 6);
    }


    public void refresh() {
        updateVariable(online_players,"§e§l"+Bukkit.getOnlinePlayers().size()+"§r§7/§a§l"+Bukkit.getMaxPlayers())
        updateVariable(playerName,"§3" + this.player.getName());
    }
}
```

# III - Appliquer le scoreboard à un joueur

Pour cela rien de plus simple quand vous avez finis de créé votre classe de scoreboard  vous devez tous  
simplement  appliquer le scoreboard aux joueurs en lignes et à ceux qui rejoignent le serveur.  
Pour cela ajoutez cette simple ligne de code *A* dans un onJoin et cette ligne de code *B* dans votre onEnable.

## A
```Java
@EventHandler
public void onJoin(PlayerJoinEvent event) {
    CScoreboardManager.instance.send(new ExempleScoreboard(event.getPlayer()));
}
```

## B
```Java
public void onEnable() {
    //Votre code...
    // new CScoreboardManager(this); <- Bien y mettre avant !
    for (Player player : Bukkit.getOnlinePlayers()) {
        CScoreboardManager.instance.send(new ExempleScoreboard(player));
    }
}
```
